<?php
    require('connect.php');
    require('functions.php');
    
    session_start();
    
    require('header.php');
 	
//if user is signed is as an admin   
    if($_SESSION['admin']){
	//if id is retrieved from GET request
        if(isset($_GET['id'])){
	//define $id as GET value
            $id=$_GET['id'];
		//prepare SQL statement to delete comments that have the id of $id
                     $stmt=$conn->prepare("DELETE FROM comments WHERE id=:id;");
		//bind $id to :id
                     $stmt->bindParam(":id", $id);
		//execute SQl statement 
                     if($stmt->execute()){
			//if it executes successfully then notify user
                         echo '<script>alert("comment delete"</script>';
                         header("delete_comments.php");
                     }
                     else {
			//if it fails then notify user that it failed 
                         echo '<script>alert("deletion failed")</script>';
                     }
                 }
                 else {
                     echo '<script>alart("not working")</script>';
                 }
    }
    	
	//if user is signed in as admin
    if(isset($_SESSION['admin'])){
	// prepare SQL statment to display all comments 
        $stmt=$conn->prepare("SELECT * FROM comments");
	//exedute SQL statement
        $stmt->execute();
        	
	//if there are rows returned
         if($stmt->rowCount()>0) {
		//fetch them and display them in rows 
                while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                    echo '<p>User : '.$row["user"].'    Title : '.$row["text"].'    Time : '.$row["time"].'<a href="delete_comments.php?id='.$row["id"].'"> [Delete]</a></p>';
                }
         }
        else {
            echo '<p1> No comments found</p>';
        }
         
    }
?>
