  <?php
    
    require('header.php');
    require('connect.php');

    session_start();
   
	//if user has clicked the submit button to enter their credentials 
    if(isset($_POST['submit'])){

    //check if login is achieved

    //get variables entered into form from POST

    $username=trim($_POST['namebox']);
    $password=trim($_POST['passwordbox']);
    $hash = password_hash($password, PASSWORD_DEFAULT);
    
    //prepare mysql statement
    $stmt= $conn->prepare("SELECT * FROM users WHERE username=:username AND password=:password");
    //bind necessary variables to mysql parameters
    $stmt->bindParam(':username',$username);
    $stmt->bindParam(':password',$password);
    //execute mysql statement
    $stmt->execute();
    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
    //if there is a match
if(password_verify($password, $hash)) {
    if($stmt->rowCount()>0){
        session_start();
        //set logged in to true
        $_SESSION['loggedin'] = true;
        if($username == "admin") {
            //set admin to true
            $_SESSION['admin'] = true;
            }
        //notify that user has logged in successfully
        ?>
        <script type="text/javascript">alert("login successful"); </script>
        <?php
        //redirect to welcome page
        header('location: welcome.php');
    }
    else {
        ?>
        <script>alert("login unsuccessful");</script>
        <?php
        header('location: login.php');
        }
}
    }
    //login form
   echo '<h2> log in </h2>

    <form name="login" action="" method="POST">
    <label>username</label><input type="text" name="namebox" length="30"><br/>
    <label>Password</label><input type="text" name="passwordbox" length="30"><br/>
    <input type="submit" name="submit">
    </form>
    
     <li><a href="register.php">not got an account?</a></li>';
     
     ?>
