<?php

require('connect.php');

session_start();

   include('header.php');
	//if user is logged in as admin
    if($_SESSION['admin']) {
        echo '<h1> create a new blog</h1>';
	
//display blog entry form
?>
	
           <form name="addblog" action="" method="POST">
                <label>title :</label><input type="text" name="titlebox"><br/>
                <label>message :</label><input type="text" name="textbox"><br/>
                <input type="submit" name="submitadd">
            </form>
            <?php
		//if submit add is clicked
               if(isset($_POST['submitadd'])){
		//define approppriate variables
                   $title=$_POST['titlebox'];
                   $text=$_POST['textbox'];
		//prepare SQL statement to insert contents into blog tbale 
                   $stmt=$conn->prepare("INSERT INTO Blog (title, text)
                   VALUES (:title, :text);");
		//bind appropriate parameters
                   $stmt->bindParam(':title',$title);
                   $stmt->bindParam(':text',$text);
                   	
			//if statement executes successfuly 
                   if($stmt->execute()){
			//display positive notification
                       ?>
                       <script>alert("blog Added");location.href="blog.php";</script>
                       <?php
                   }
                   else {
			//display negative notification
                       ?>
                       <script>alert("blog not added");location.href="blog.php";</script>
                       <?php
                   }
            }
    }
    else {
	//else if user is not signed in as admin then notify user 
        echo '<script>alert("only admin can create blogs")</script>';
        echo '<li><a href="blog.php">Back to blog page</a></li>';
    }
    include('footer.php');

?>
