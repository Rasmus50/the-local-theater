<?php 
include('functions.php');
include('connect.php');
include('header.php');

echo '<h1> create a new user</h1>';

?>
        /* registration form user can fill in */

           <form name="register" action="" method="POST">
                <label>Username :</label><input type="text" name="namebox"><br/>
                <label>Password :</label><input type="text" name="passbox"><br/>
                <label>Email :</label><input type="text" name="emailbox"><br/>
                <input type="submit" name="register">
            </form>
            <?php
	// once user clicks register button
               if(isset($_POST['register'])){
		// copy user input from POST into appropriate variables
                   $username=$_POST['namebox'];
                   $password=$_POST['passbox'];
                   $email=$_POST['emailbox'];
			//set user as active
                   $active=1;
			//prepare SQL to statement to insert new user into users table
                   $stmt=$conn->prepare("
                   INSERT INTO users (username, password, email, active) VALUES (:username, :password, :email, :active);");
		//bind appropreiate parameters
                   $stmt->bindParam(':username',$username);
                   $stmt->bindParam(':password',$password);
                   $stmt->bindParam(':email',$email);
                   $stmt->bindParam(':active', $active);
			
			//if statement executes sucesfully  then notify user
                   if($stmt->execute()){
                       ?>
                       <script>alert("Record Added");location.href="index.php";</script>
                       <?php
                   }
               }

?>
