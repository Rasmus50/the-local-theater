<?php 
    require('connect.php');
    require('functions.php');
    
    session_start();
    
    require('header.php');
    
    $id = $_GET['id'];
    
    $stmt=$conn->prepare("SELECT * FROM Blog WHERE id=:id");
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    ?>
    <section id="blog">
    <div class="center">
        <h1>Blog Post</h1>
        <?php
    while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                //output the row
                echo '<p>ID : '.$row["id"].'Title :'.$row["title"].' Time : '.$row["time"].'</p>';
                echo '<p>'.$row['text'].'</p>';
                
            }
    ?>
    </section>
    </div>
    
    <section id="comments">
    <div class="center">
        <h2>Comments</h2>
        <?php
            $stmt=$conn->prepare("SELECT * FROM comments WHERE blog=:id");
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            
            if($stmt->rowCount()>0) {
                while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                    echo '<p>User : '.$row["user"].'    Title : '.$row["text"].'    Time : '.$row["time"].'<a/><p>';
                 }
            }
          ?>  
    </div>
    </section>
    <?php
            if(isset($_SESSION['loggedin'])) {
                ?>
                
                <div id="comment">
                <h1>Add a comment</h1>
                
                
                    <form action="" method="post" id="commentform">
<div class="center">
 <label for="username" class="required">Your username</label>
    <input type="text" name="namebox" id="username" value="" tabindex="1" required="required">
    
    <label for="password" class="required">Your password</label>
    <input type="text" name="passbox" id="password" value="" tabindex="2" required="required">

    <label for="comment" class="required">Your message</label>
    <textarea name="comment" id="comment" rows="10" tabindex="4"  required="required"></textarea>

    <input name="submit" type="submit" value="Submit comment" />

  </form>
  </div>
  </div>
  <?php
        if(isset($_POST['submit'])){
            $username=$_POST['namebox'];
            $password=$_POST['passbox'];
            $comment=$_POST['comment'];
            
            $stmt=$conn->prepare("SELECT * FROM users WHERE username=:username AND password=:password");
            $stmt->bindParam(':username',$username);
            $stmt->bindParam(':password',$password);
            $stmt->execute();
            if($stmt->rowCount()>0){
             $stmt=$conn->prepare("INSERT INTO comments (blog, user, text) VALUES (:id, :user, :comment);");
             $stmt->bindParam(":id", $id);
             $stmt->bindParam(":user", $username);
             $stmt->bindParam(":comment", $comment);
                if($stmt->execute()) {
                    echo '<script>alert("comment added")</script>';
                }
                else {
                    echo '<script>alert("comment failed")</script>';
                }
            }
            else {
                echo '<script>alert("incorrect details")</script>';
            }
                }
            }
            
            ?>
    </div>
    <?php
            require('footer.php');
            ?>
            