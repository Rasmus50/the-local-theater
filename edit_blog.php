<?php 

require('connect.php');

require('functions.php');

session_start();

require('header.php');

//if user is logged in as admin
if(isset($_SESSION['admin'])) {
	//if edit button is clicked on preivious page
    if(isset($_GET['edit'])) {
	//call edit blog function
        edit_blog();
    }
	//define variabels used in SQL sstatement 
    $id=$_GET['id'];
	//prepare SQL statement 
    $stmt=$conn->prepare("SELECT * FROM Blog WHERE id =:id");
	//bind $id to :id
    $stmt->bindParam(":id", $id);
	//exeute SQL statement
    $stmt->execute();
    	
//if there is content returned from the databas3
    if($stmt->rowCount()>0){
	//fetch the contents and display them in a form that the user can edit
        while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
            echo '
            <form name="edit" action="" method="GET">
            <input name="id" type="hidden" value="'.$row['id'].'">
            <input name="titlebox" value="'.$row['title'].'">
            <input name="textbox" value="'.$row['text'].'">
            <input type="submit" name="edit">
            </form>';
        }
    }
    else {
	//else if there is nothing found then return "no records found"
        echo '<p1> No records found</p>';
    }
    //if edit is clicked on: SELECT * FROM blog WHERE id=recordid (retrieve the specified blog post by using its id)
    //display the record inside a form that the user can edit
    //send it back to database with pass or fail
    }
else {
    header('blog.php');
}
require('footer.php');
?>
