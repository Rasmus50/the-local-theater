<!DOCTYPE html>
<html>
<head>
    <title>The local theatre</title>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <meta name="viewport" content="width=device-width" />
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>
	<body>
	
		<nav>
    <style type="text/css">
    body {
    background-color: #000000;
    color : #FFFFFF;
    }
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #000000;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.center {
  line-height: 2;
  height: 4;
  border: 3px solid #660000;
  text-align: center;
}
@media only screen and (max-width: 600px) {
 nav li {
   display: inline;
   font-size: 1.5em;
   padding: 10px;
 }
}

.active {
  background-color: #7F0000;
}
    </style>
</head>
<body>
    <div>
        <a href="index.php">                <div style="text-allign: center"> 
            <img src="mainlogo.png" alt="The local theatre logo">
            </div>    
        </a>
        <ul>
            <li><a href="index.php">Home</a></li>
            <?php
            session_start();
            if($_SESSION['loggedin']) {
            ?>
            <li><a href="logout.php">Logout</a></li>   
            <?php
            }
            ?>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="contact_us.php">Contact us</a></li>
            <?php
            if($_SESSION['admin']){
                ?>
              <li><a href="users.php">View Users</a></li>
              <li><a href="delete_comments.php">Delete comments</a></li>
              <?php
            }
            if(!$_SESSION['loggedin']){
                ?>
                <li style="float:right"><a class="active" href="login.php">Log in</a></li>
                <li style="float:right"><a class="active" href="register.php">Sign up</a></li>
                <?php
            }
            ?>
        </ul>
    </div>
    <div id="main">

