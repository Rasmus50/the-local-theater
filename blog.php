<?php 

require('connect.php');

session_start();

require('header.php');

?>

    <h1> This is the Blog</h1>
    
<?php
	
	//if user is logged in as admin
    if($_SESSION['admin']){
	//if id is retrieved from POST function by GET
        if(isset($_GET['id'])){
		//set appropriate variable value
            $id=$_GET['id'];
		//prepare SQL statement 
            $stmt=$conn->prepare("DELETE FROM Blog WHERE id=:id");
		//bind id parameter to search for 
            $stmt->bindParam(':id',$id);
		//execute statement
            $stmt->execute();
        }
    }
	//prepare SQL statement to get contents from blog table
        $stmt=$conn->prepare("SELECT * FROM Blog");
        $stmt->execute();
	//if there are actually blogs
        if($stmt->rowCount()>0){
                //output the each blog as a row 
                while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                        echo '<div class="center"><p>ID : '.$row["id"].'Title :'.$row["title"]. $row["text"].$row["time"].'<a href="blog.php?id='.$row["id"].'"> [Delete] </a>
                        <a href="edit_blog.php?id='.$row["id"].'">[Edit]</a> <a href="view.php?id='.$row["id"].'"> [View] </a></p></div>';
                }
        }
        else {
        echo "<p> No records found</p>";
        }
		//if user is logged in then display button to compose a blog entry 
            if($_SESSION['loggedin']) {
            ?>
            <div>
            <li><a href="createblog.php">Compose a blog entry</a></li>   
            </div>
            <?php
            }
        require("footer.php");
?>
