<?php

function loginCheck(){
    
/* function checks if user has a saved session and retrieves login credentials from cookies to compare*/
        global $conn;
      
	//if user has clicked submit on login form 
        if(isset($_POST['submit'])){
            //check if login is achieved
            //get variables from form
		//define username and password as values retrieved from form
            $username=$_POST['namebox'];
            $password=$_POST['passwordbox'];
	//prepare SQL statement to retrieve the user details
            $stmt= $conn->prepare("SELECT * FROM users WHERE
            username =:username AND password=:password");
            $stmt->bindParam(':username',$username);
            $stmt->bindParam(':password',$password);
            $stmt->execute();
	//if there are matching records and username is admin
            if($stmt->rowCount()>0 && $username == "admin"){
                echo "<p>Login successful</p>";
	        //sign user in as admin        
		$_SESSION['admin']=true;
            }
		//else if just matching credentials with no admin
            elseif ($stmt->rowCount()>0) {
		//notify user
                echo "<p>Login successful</p>";
            }
            else {
//else if no matching records are found
                echo "<p>no record exists";
            }
        }
}

function edit_blog(){

	/* functions takes values from form and updates blog entry using them */
            global $conn;
            	
		//define variables to use in statement as values retrieved from form
            $id     = $_GET['id'];
            $title      = $_GET['titlebox'];
            $text       = $_GET['textbox'];
		//prepare SQL statement to update the blog entry
            $stmt=$conn->prepare("UPDATE Blog
                                    SET title =:title,
                                    text    =:text
                                    WHERE id =:id;");
		//bind appropriate values
            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":text", $text);
		//if statement exeuctes successfuly
            if($stmt->execute()){
			//notify user
                       ?>
                       <script>alert("Blog Amended");location.href="blog.php";</script>
                       <?php
             }
             else {
		//else if statement fails notify user
                 ?>
                <script>alert("Fail")</script>
                 <?php

             }

}

function activate() {

	/* function to activate users that have been deactivated by admin */

    global $conn;
    
	//define $id by GET request value from previous page
    $id=$_GET['id'];
	//prepare statemen to change user from active to inactive
    $stmt=$conn->prepare("UPDATE users SET active=1 WHERE id=:id;");
	//bind :id to $id 
    $stmt->bindParam(":id", $id);
	//if statement executes succesffuly then notify user
    if($stmt->execute()){
        ?>
        <script>alert("user activated")</script>
        <?php
    }
    else {
	//if statement fails then noitfy user
        ?>
        <script>alert("fail")</script>;
        <?php
        }
    }


function deactivate() {

	/*function to deactivate users to stop them from logging in */
    global $conn;
    
	//define $id by GET request value from previous page
    $id=$_GET['id'];
	//prepare SQL statement to convert active = 1 to active = 0
    $stmt=$conn->prepare("UPDATE users SET active=0 WHERE id=:id;");
    $stmt->bindParam(":id", $id);
if($stmt->execute()){
	//if statement executes statement successfully notfy user
    ?>
    <script>alert("user deactivated")</script>
    <?php
}
else {
	// othewise notify user that it failed
    ?>
    <script>alert("fail")</script>;
    <?php
    }
}
