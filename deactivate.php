<?php

require('connect.php');
require('functions.php');

session_start();

//if user is logged in as an admin
if($_SESSION['admin'] == true) {
    require('header.php');
	//call deactivate function
    deactivate();
    require('footer.php');
}
